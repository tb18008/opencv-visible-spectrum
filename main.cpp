//! [includes]
#include <cmath>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <vector>

#include <iostream>

using namespace std;
using namespace cv;
//! [includes]


/*
 *  Returns interpolated value at x from parallel arrays ( xData, yData )
 *
 *  Assumes that xData has at least two elements, is sorted and is strictly monotonic increasing
 *  boolean argument extrapolate determines behaviour beyond ends of array (if needed)
 *  http://www.cplusplus.com/forum/general/216928/
 */
double interpolate( vector<double> &xData, vector<double> &yData, double x, bool extrapolate )
{
    int size = xData.size();

    int i = 0;                                                                  // find left end of interval for interpolation
    if ( x >= xData[size - 2] )                                                 // special case: beyond right end
    {
        i = size - 2;
    }
    else
    {
        while ( x > xData[i+1] ) i++;
    }
    double xL = xData[i], yL = yData[i], xR = xData[i+1], yR = yData[i+1];      // points on either side (unless beyond ends)
    if ( !extrapolate )                                                         // if beyond ends of array and not extrapolating
    {
        if ( x < xL ) yR = yL;
        if ( x > xR ) yL = yR;
    }

    double dydx = ( yR - yL ) / ( xR - xL );                                    // gradient

    return yL + dydx * ( x - xL );                                              // linear interpolation
}


/*
 * Tristimulus X, Y and Z values calculated for given wavelength.
 * Color matching functions approximated as sum of Gaussian functions similar to CIE standard observer
 * color matching functions
 * https://en.wikipedia.org/wiki/CIE_1931_color_space#Color_matching_functions
 */

double gaussian(double x, double scale, double peak_x, double st_dev_left, double st_dev_right) {
    double t = (x - peak_x) / (x < peak_x ? st_dev_left : st_dev_right);
    return scale * exp(-(t * t) / 2);
}

// Sums the values from specified gaussian functions at given wavelength
void wavelengthToXYZ(int wavelength, double xyz[3]) {

    wavelength *= 10;   //Converts nanometers to 1 × 10^(-8) m

    xyz[0] = gaussian(wavelength,  1.056, 5998, 379, 310) +
              gaussian(wavelength,  0.362, 4420, 160, 267) +
              gaussian(wavelength, -0.065, 5011, 204, 262);
    xyz[1] = gaussian(wavelength,  0.821, 5688, 469, 405) +
              gaussian(wavelength,  0.286, 5309, 163, 311);
    xyz[2] = gaussian(wavelength,  1.217, 4370, 118, 360) +
              gaussian(wavelength,  0.681, 4590, 260, 138);
}

// Does matrix multiplication (rgb = conv_matrix × xyz)
void XYZtoRGB(double conv_matrix[][3], const double xyz[3], double rgb[3])
{
    for (int i = 0; i < 3; i++) {
        rgb[i] = 0;
        for (int j = 0; j < 3; j++) rgb[i] += conv_matrix[i][j] * xyz[j];
    }
}

//Corrects R, G or B value using gamma correction
//https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation
double gammaCorrect(double u){
    if(u < 0.0031308) return 323 * u / 25;
    else return (211 * pow(u, (float)5/12) - 11) / 200;
}

int main()
{


    //GENERATE RGB DATA

    //XYZ to sRGB for illuminant D65
    //https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation
    double conversion_matrix[3][3] = {
            { 3.2404532, -1.5371385, -0.4985314 },
            { -0.9692660, 1.8760108, 0.0415560 },
            { 0.0556434, -0.2040259, 1.0572252 }
    };
    //XYZ calculated from wavelength
    double tristimulus_xyz[3];
    //Resulting color
    double rgb[3];

    vector<double> wavelengths(400);
    vector<double> reds(400);
    vector<double> greens(400);
    vector<double> blues(400);

    //Populate arrays with wavelength color data
    for (int i = 380; i < 780; ++i) {

        // Get XYZ for wavelength i
        wavelengthToXYZ(i, tristimulus_xyz);

        // Get RGB for XYZ
        XYZtoRGB(conversion_matrix, tristimulus_xyz, rgb);

        // Add data to arrays
        wavelengths.push_back (i);
        reds.push_back (gammaCorrect(rgb[0]));
        greens.push_back (gammaCorrect(rgb[1]));
        blues.push_back (gammaCorrect(rgb[2]));

    }



    //INTERPOLATE DATA AND DRAW IMAGE

    //Image related variables
    int img_width, img_height = 0;
    bool invalid = true;

    // Write info about program

    cout << "Draws visible spectrum in CIE 1931 colorspace using OpenCV:"<<endl;
    cout << "\t 1) Calculates RGB color data for each wavelength (in nanometers), wavelength -> XYZ -> RGB" << endl;
    cout << "\t 2) After user input, draws image with given dimensions" << endl;
    cout << "\t 3) Applies gamma correction to RGB values" << endl;
    cout << "\t 4) Draws 1 px wide lines using interpolated values from spectrum data" << endl;
    cout << "Note: When Visible Spectrum image window appears, press 'S' key to save image at executable URL (file name 'visible_spectrum.png')" << endl;

    // Get input and validate
    while(invalid){
        cout << "Enter image width (400 - 2000):" << endl;
        cin >> img_width;
        if(img_width >= 400 && img_width <= 2000){
            cout << "Enter image height (1 - 2000):" << endl;
            cin >> img_height;
            if(img_height >= 1 && img_height <= 2000) invalid = false;
        }
    }

    //Create black image with user specified dimensions
    Mat img(img_height,img_width, CV_8UC3, Scalar(0, 0, 0 ));

    double position_x;
    double color[3] = {0, 0, 0};

    for (int i = 0; i < img_width; ++i) {

        position_x = (float)i / img_width * 400 + 380;

        color[0] = interpolate( wavelengths, reds, position_x, true);
        color[1] = interpolate( wavelengths, greens, position_x, true);
        color[2] = interpolate( wavelengths, blues, position_x, true);

        //Draw line on the black image, color given as scalar with BGR values
        line(img, Point(i, 0), Point(i, img_height), Scalar(256 * color[2], 256 * color[1], 256 * color[0] ), 1, LINE_4);

    }

    //! [imshow]
    imshow("Visible spectrum", img);
    int k = waitKey(0); // Wait for a keystroke in the window
    //! [imshow]

    //! [imsave]
    if(k == 's')
    {
        imwrite("visible_spectrum.png", img);
    }
    //! [imsave]

    return 0;
}